'use strict';

import React, {Component} from 'react';
import ReactDOM from 'react-dom';

import { createStore, combineReducers, applyMiddleware } from 'redux';
// import { Provider } from 'react-redux';
import { Provider, intlReducer } from 'react-intl-redux'

import createHistory from 'history/createBrowserHistory';
import { ConnectedRouter, routerMiddleware, routerReducer } from 'react-router-redux';
import BrowserRouter from 'react-router-dom/BrowserRouter';
import { renderRoutes } from 'react-router-config';

import {IntlProvider} from 'react-intl';
// import * as translations from './translations';
import { addLocaleData } from 'react-intl';
import ruTranslation from './translations/ru'
import ru from 'react-intl/locale-data/ru';

import routes from './utils/routes';
import * as reducers from './reducers';

const history = createHistory();
const middleware = routerMiddleware(history);

const state = window.__PRELOADED_STATE__;

addLocaleData([...ru]);
state.intl = {
    locale: 'ru',
    messages: ruTranslation
};

const store = createStore(combineReducers({
    contributors: reducers.common([]),
    backgrounds: reducers.common([]),
    service: reducers.service,
    services: reducers.common([]),
    csrf: reducers.common([]),
    menu: reducers.common([]),
    message: reducers.message,
    contributor: reducers.contributor,
    tags: reducers.tags,
    article: reducers.article,
    router: routerReducer,
    intl: intlReducer
}), state, applyMiddleware(middleware));


// TODO

// const locale = 'ru';

// end todo

document.onreadystatechange = function () {
    if (document.readyState !== 'complete') {
        return;
    }
    ReactDOM.render(
        // <IntlProvider locale={locale} messages={translations[locale]}>
            <Provider store={store}>
                <BrowserRouter>
                    {renderRoutes(routes)}
                </BrowserRouter>
            </Provider>,
        // </IntlProvider>,
        document.getElementById('content')
    );
};
