'use strict';

import sample from 'lodash/fp/sample';
import React, { Component } from 'react';
import { connect } from 'react-redux';

require('./mono-col.styl');

class MonoCol extends Component {
    render() {
        return (<div className="mono-col" style={this.props.background}>
            <div className="mono-col__wrapper">
                { this.props.header ? <h1 className="mono-col__header">{this.props.header}</h1> : '' }
                <div className="mono-col__text">{ this.props.children }</div>
            </div>
        </div>);
    }
}

export default connect(store => ({
    background: { backgroundImage: `url(${sample(store.backgrounds)})` }
}))(MonoCol);
