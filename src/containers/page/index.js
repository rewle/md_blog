'use strict';

import React, { Component } from 'react';
import { withRouter } from 'react-router';
import { Link } from 'react-router-dom';
import { renderRoutes } from 'react-router-config';

import { connect } from 'react-redux';
import Menu from '../../components/menu';

require('./page.styl');

class Page extends Component {
    render() {
        return (<div>
            <div className="page__header">
                <div className="page__col page__col_left">
                    <Link className="logo" to="/"/>
                </div>
                <div className="page__col page__col_right">
                    <Menu {...this.props}/>
                </div>
            </div>
            <div className="page__content">
                {renderRoutes(this.props.route.routes)}
            </div>
        </div>);
    }
}

export default withRouter(connect(store => {
    return {
        menu: store.menu,
        contributors: store.contributors
    };
})(Page));
