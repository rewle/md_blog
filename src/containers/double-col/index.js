'use strict';

import React, { Component } from 'react';

import LeftCol from './left-col';
import RightCol from './right-col';
import Alert from '../../components/alert';

require('./double-col.styl');

class DoubleCol extends Component {
    render() {
        return (<div className="double-col">
            <Alert direction="right" />
            <LeftCol header={this.props.leftTilte} items={this.props.leftItems}>
                {this.props.left}
            </LeftCol>
            <RightCol>
                {this.props.right}
            </RightCol>
        </div>);
    }
}

export default DoubleCol;
