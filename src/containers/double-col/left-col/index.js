'use strict';

import sample from 'lodash/fp/sample';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

require('./left-col.styl');

class LeftCol extends Component {
    render() {
        return (<div className="left-col" style={this.props.background}>
            <div className="left-col__wrapper">
                {
                    this.props.header ? <h1 className="left-col__header">{this.props.header}</h1> : ''
                }
                {
                    Array.isArray(this.props.items) ? this.props.items.map((item, index) => {
                        let className = 'left-col__item';
                        if (item.current) {
                            className += ' left-col__item_current';
                        }

                        return (<Link to={item.href} className={className} key={index}>{item.node}</Link>);
                    }) : ''
                }
                {
                    this.props.children
                }
            </div>
        </div>);
    }
}

export default connect(store => ({
    background: { backgroundImage: `url(${sample(store.backgrounds)})` },
}))(LeftCol);
