'use strict';

import React, { Component } from 'react';

require('./right-col.styl');

class RightCol extends Component {
    render() {
        return (<div className="right-col">
            <div className="right-col__wrapper">
                {this.props.children}
            </div>
        </div>);
    }
}

export default RightCol;
