import Page from '../containers/page';
import Contributors from '../components/contributors';
import Contributors2Person from '../components/contributors2person';
import Blog from '../components/blog';
import Blog2Article from '../components/blog2article';
import Services from '../components/services';
import About from '../components/about';

const routes = [
    {
        component: Page,
        routes: [
            {
                path: '/',
                exact: true,
                component: About
            },
            {
                path: '/blog',
                exact: true,
                component: Blog2Article
            },
            {
                path: '/blog/:article',
                component: Blog
            },
            {
                path: '/services',
                exact: true,
                component: Services
            },
            {
                path: '/services/:service',
                component: Services
            },
            {
                path: '/contributors',
                exact: true,
                component: Contributors2Person
            },
            {
                path: '/contributors/:login',
                component: Contributors
            }
        ]
    }
];

export default routes;
