'use strict';

module.exports = titleParts => {
    try {
        document.title = ['ReWle.net'].concat(titleParts).join(' — ');
    } catch (e) {}
};
