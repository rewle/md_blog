'use strict';

const React = require('react');
const Head = require('./components/head');

module.exports = function (state) {
    return <Head state={state}/>;
};
