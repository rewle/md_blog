'use strict';

import 'whatwg-fetch';
import * as constants from '../constants';

export function menu(dispatch) {
    fetch('/api/articles').then(res => {
        if (res.status >= 400) {
            throw new Error('Error in response from server');
        }
        return res.json();
    }).then(menuJSON => {
        dispatch({
            type: constants.MENU_FETCHED,
            menu: menuJSON
        });
    });
}
