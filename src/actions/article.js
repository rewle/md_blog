'use strict';

import 'whatwg-fetch';
import * as constants from '../constants';

export function article(id, dispatch) {
    dispatch({
        type: constants.ARTICLE_FETCHING,
        article: {}
    });

    fetch(`/api/articles/${id}`).then(res => {
        if (res.status >= 400) {
            throw new Error('Error in response from server');
        }

        return res.json();
    }).then(article => {
        dispatch({
            type: constants.ARTICLE_FETCHED,
            article
        });
    });
}
