'use strict';

import 'whatwg-fetch';
import * as constants from '../constants';

export function service(form, noSubmit, dispatch) {
    const validName = form.name && form.name.trim() || (noSubmit && !('name' in form));
    const validEmail = form.email && form.email.trim() || (noSubmit && !('email' in form));
    if (!validName || !validEmail || noSubmit) {
        dispatch({
            type: constants.SERVICE_ERROR,
            service: {
                invalid: {
                    name: !validName,
                    email: !validEmail
                }
            }
        });
        return;
    }
    dispatch({
        type: constants.SERVICE_SENDING,
        service: {
            sending: true
        }
    });

    fetch(`/api/task?_csrf=${form.csrf}`, {
        method:'POST',
        body: JSON.stringify(form),
        credentials: 'include',
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(res => {
        if (res.status >= 400) {
            return {error: true};
        }
        return res.json()
    }).then(response => {
        dispatch({
            type: constants.SERVICE_SENDED,
            service: {
                sending: false
            }
        });
        dispatch({
            type: constants.MESSAGE,
            message: {
                text: response.error ? 'services:form:fail' : 'services:form:send',
                type: response.error ? 'error' : 'success'
            }
        });
    }, err => {
        dispatch({
            type: constants.SERVICE_SENDED,
            service: {
                sending: false
            }
        });
        dispatch({
            type: constants.MESSAGE,
            message: {
                text: 'services:form:fail',
                type: 'error'
            }
        });
    });
}
