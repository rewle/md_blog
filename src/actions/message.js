'use strict';

import 'whatwg-fetch';
import * as constants from '../constants';

export function message(msg, dispatch) {
    dispatch({
        type: constants.MESSAGE,
        message: msg
    });
}
