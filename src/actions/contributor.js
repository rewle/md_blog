'use strict';

import 'whatwg-fetch';
import * as constants from '../constants';

export function contributor(login, dispatch) {
    dispatch({
        type: constants.CONTRIBUTOR_FETCHING,
        description: '',
        login
    });

    fetch(`/api/users/${login}`).then(res => {
        if (res.status >= 400) {
            throw new Error('Error in response from server');
        }
        return res.json();
    }).then(res => {
        dispatch({
            type: constants.CONTRIBUTOR_FETCHED,
            description: res.description,
            login
        });
    });
}
