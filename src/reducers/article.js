'use strict';

import * as constants from '../constants';

const initialState = {};

export function article(state = initialState, action) {
    if (action.type === constants.ARTICLE_FETCHED) {
        return action.article;
    }
    return state;
}
