'use strict';

import * as constants from '../constants';

const initialState = {
    type: 'empty'
};

export function message(state = initialState, action) {
    if (action.type === constants.MESSAGE) {
        return action.message;
    }
    return state;
}
