'use strict';

import * as constants from '../constants';

const initialState = {
    description: '',
    login: ''
};
const types = [
    constants.CONTRIBUTOR_FETCHING,
    constants.CONTRIBUTOR_FETCHED
];

export function contributor(state = initialState, action) {
    if (types.indexOf(action.type) !== -1) {
        return {
            description: action.description,
            login: action.login
        };
    }
    return state;
}
