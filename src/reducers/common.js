'use strict';

export function common(initialState) {
    return state => (state || initialState);
}
