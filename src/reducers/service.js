'use strict';

import * as constants from '../constants';

const initialState = {};

export function service(state = initialState, action) {
    if (action.type === constants.SERVICE_SENDING ||
        action.type === constants.SERVICE_SENDED ||
        action.type === constants.SERVICE_ERROR) {

        return action.service;
    }
    return state;
}
