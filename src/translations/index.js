'use strict';

import { addLocaleData } from 'react-intl';
import ru from 'react-intl/locale-data/ru';
import ruTranslation from './ru';

addLocaleData([...ru]);

module.exports = {
    ru: ruTranslation
};
