'use strict';

import React from 'react';
import Copy from './components/copy';
import Counters from './components/counters';
import manager from 'react-intl-modules-loader/manager';
import { IntlProvider } from 'react-intl';

const locale = 'ru';


module.exports = function (props, client) {
    return (<IntlProvider locale={locale} messages={manager.getLocals(locale)}>
        <div className="page">
            <div className="page__wrapper">
                <div id="content" className="page__main" dangerouslySetInnerHTML={{__html: client}}/>
                <div className="page__footer">
                    <Copy/>
                </div>
            </div>
            <Counters/>
        </div>
    </IntlProvider>);
};
