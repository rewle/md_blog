export const CONTRIBUTOR_FETCHING = 'CONTRIBUTOR_FETCHING';
export const CONTRIBUTOR_FETCHED = 'CONTRIBUTOR_FETCHED';
export const MENU_FETCHED = 'MENU_FETCHED';
export const ARTICLE_FETCHING = 'ARTICLE_FETCHING';
export const ARTICLE_FETCHED = 'ARTICLE_FETCHED';
export const SERVICE_SENDING = 'SERVICE_SENDING';
export const SERVICE_SENDED = 'SERVICE_SENDED';
export const SERVICE_ERROR = 'SERVICE_ERROR';
export const MESSAGE = 'MESSAGE';
