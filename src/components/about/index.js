'use strict';

import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';

import MonoCol from  '../../containers/mono-col';
import title from '../../utils/title';

require('./about.styl');

class About extends Component {
    componentDidMount () {
        title(this.props.intl.formatMessage({ id: 'about:title' }));
    }
    render() {
        return (<MonoCol header="ReWle.net">
            <p className="about__text">ReWle &mdash; это небольшое консалтинговое агенство.</p>
            <p className="about__text">Наша миссия &mdash; систематезировать идеи и действительность, открывая скрытые хаосом детали, показывая картину целиком.</p>
            <p className="about__text">На этом сайте вы найдёте небольшие <Link to="/blog">заметки</Link> о том как анализировать процессы на предприятии, ставить цели, работать с неизвестостью и многое другое. Также, если изложенные в заметках подходы покажутся вам интересными, на сайте присутствует раздел <Link to="/services">услуги</Link>. В нём вы сможете обратиться за помощью в решении вашей задачи.</p>
            <p className="about__text">Также планируется организовать песочницу, в которой будет происходить разбор собственных мини-проектов, ошибок и возникающих проблем.</p>
        </MonoCol>);
    }
}

export default injectIntl(About);
