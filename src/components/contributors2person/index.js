'use strict';

import React, { Component } from 'react';
import { Route, Redirect} from 'react-router-dom';

import { connect } from 'react-redux';

class Contributors2Person extends Component {
    render() {
        const login = this.props.contributors[0].login;
        return (<Route render={({ staticContext }) => {
            if (staticContext) {
                staticContext.status = 302;
            }

            return <Redirect to={`/contributors/${login}`}/>;
        }}/>);
    }
}

export default connect(store => {
    return {
        contributors: store.contributors
    };
})(Contributors2Person);
