'use strict';

import React, { Component } from 'react';

export default class Social extends Component {
    componentDidMount() {
        window.Ya.share2(document.getElementById('article-share'));
    }

    render() {
        return <div id="article-share" data-services="vkontakte,facebook,twitter,linkedin" data-counter=""/>;
    }
}
