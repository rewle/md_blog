'use strict';

import React, { Component } from 'react';

require('./counters.styl');

const counterCode = `(function() {(
    window["yandex_metrika_callbacks"] = window["yandex_metrika_callbacks"] || []).push(function() {
        try {
            window.yaCounter44052944 = new Ya.Metrika({
                id:44052944,
                clickmap:true,
                trackLinks:true,
                accurateTrackBounce:true,
                webvisor:true
            });
        } catch(e) {
            console.error('Counter:', e);
        }
    });

    var n = document.getElementsByTagName("body")[0],
        s = document.createElement("script"),
        f = function () { n.parentNode.append(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = "//mc.yandex.ru/metrika/watch.js";

    if (window.opera == "[object Opera]") {
        document.addEventListener("DOMContentLoaded", f, false);
    } else {
        f();
    }
})();`;

export default class Counters extends Component {

    render() {
        return (<div>
            <noscript>
                <img className="counters__noscript" src="//mc.yandex.ru/watch/44052944" alt=""/>
            </noscript>
            <script dangerouslySetInnerHTML={{ __html: counterCode }}/>
        </div>);
    }
}
