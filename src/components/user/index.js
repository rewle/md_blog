/* eslint react/no-danger: 0 */
'use strict';

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import get from 'lodash/fp/get';
import findLast from 'lodash/fp/findLast';
import { contributor } from '../../actions';
import Spinner from '../spinner';

require('./user.styl');

class User extends Component {
    _getContributor(params, login) {
        const { actions } = this.props;
        if (login !== params.login) {
            actions.contributor(params.login);
        }
    }

    componentDidMount() {
        this._getContributor(this.props.params, this.props.login);
    }

    componentWillReceiveProps(nextProps) {
        this._getContributor(nextProps.params, nextProps.login);
    }

    render() {
        if (!this.props.description) {
            return (<div className="user user_loading">
                <Spinner/>
            </div>);
        }
        return <div className="user" dangerouslySetInnerHTML={{ __html: this.props.description }}/>;
    }
}

export default withRouter(connect((store, own) => {
    return {
        params: own.match.params,
        login: store.contributor.login,
        description: store.contributor.description,
        name: get(
            findLast(store.contributors, item => (item.login === store.contributor.login)),
            'fullName'
        )
    };
}, dispatch => ({
    actions: {
        contributor: login => contributor(login, dispatch)
    }
}))(User));
