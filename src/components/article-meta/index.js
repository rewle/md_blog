'use strict';

import React, { Component } from 'react';
import Share from '../share';
import Pager from '../pager';

require('./article-meta.styl');

export default class ArticleMeta extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tab: ''
        };
    }

    tab(tabName) {
        this.setState({ tab: tabName });
    }

    render() {
        const className = `article__footer`;

        return (<div className={className}>
            <div className="article__controls">
                <Share/>
                <Pager/>
            </div>
        </div>);
    }
}
