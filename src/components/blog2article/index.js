'use strict';

import React, { Component } from 'react';
import { Route, Redirect} from 'react-router-dom';

import { connect } from 'react-redux';

const last = tree => {
    let stack = [].concat(tree);
    let index = 0;

    while (stack.length) {
        const node = stack.shift();

        if (Array.isArray(node.content)) {
            stack = stack.concat(node.content);
        }

        if (node.id > index) {
            index = node.id;
        }
    }

    return index;
};

class Blog2Article extends Component {
    render() {
        const menu = this.props.menu;
        return (<Route render={({ staticContext }) => {
            if (staticContext) {
                staticContext.status = 302;
            }
            return <Redirect to={`/blog/${last(menu)}`}/>;
        }}/>);
    }
}

export default connect(store => {
    return {
        menu: store.menu
    };
})(Blog2Article);
