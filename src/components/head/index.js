'use strict';

const React = require('react');

module.exports = class Head extends React.Component {
    render() {
        const pageStyle = `/static/${this.props.state.page}.css`;
        const pageScript = `/static/${this.props.state.page}.js`;

        return (<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# product: http://ogp.me/ns/product#">
            <script async src="https://www.googletagmanager.com/gtag/js?id=UA-106710904-1"></script>
            <script dangerouslySetInnerHTML={{ __html: `
                window.dataLayer = window.dataLayer || [];
                function gtag(){dataLayer.push(arguments)};
                gtag('js', new Date());

                gtag('config', 'UA-106710904-1');
            `}}></script>

            <meta httpEquiv="Content-Type" content="text/html; charset=utf-8"/>
            <meta httpEquiv="X-UA-Compatible" content="IE=edge"/>
            <meta name="viewport" content="width=device-width, initial-scale=1 maximum-scale=1"/>
            <meta name="google-site-verification" content="-rY_BUn2MxLLNnFDMfS-8O3OQkpX8BG3kaVJWzs48yw"/>

            <meta name="description" content="Частные консалтинговые услуги для IT предприятий"/>
            <meta itemprop="image" content="/static/i/logo.png" />

            <meta property="fb:app_id" content="1947921262141190" />
            <meta property="og:locale" content="ru_RU" />
            <meta property="og:title" content="ReWle.net"/>
            <meta property="og:description" content="Частные консалтинговые услуги для IT предприятий"/>
            <meta property="og:url" content="http://rewle.net" />
            <meta property="og:type" content="http://rewle.net/static/i/logo.png" />
            <meta property="og:type" content="product.group" />


            <meta name="google" content="nositelinkssearchbox" />

            <link rel="stylesheet" href="/static/client.css"/>
            <link href="//fonts.googleapis.com/css?family=PT+Sans+Narrow" rel="stylesheet"/>
        </head>);
    }
};
