'use strict';

import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';

require('./pager.styl');

const texts = {
    prev: 'Туда',
    next: 'Сюда'
};

class Pager extends Component {
    pager() {
        const current = this.props.articleId;
        const menu = this.props.menu;

        let volume = 0;
        let article = 0;
        let prev = null;
        let next = null;
        let findCurrent = false;

        while (menu[volume]) {
            const item = menu[volume].content[article];

            if (!item) {
                article = 0;
                volume += 1;
                continue;
            }

            if (item.id === current) {
                findCurrent = true;
                article += 1;
                continue;
            }

            if (findCurrent) {
                next = item;
                break;
            } else {
                prev = item;
                article += 1;
            }
        }

        return {
            prev, next
        };
    }

    render() {
        if (!this.props.menu.length || !this.props.articleId) {
            return <div className="pager"/>;
        }

        const pager = this.pager();
        return (<div className="pager">
            {
                ['prev', 'next'].map((item, index) => {
                    const className = `pager__nav-link pager__${item}`;

                    const pointer = pager[item];
                    if (!pager[item]) {
                        return '';
                    }

                    return (<Link to={pager[item].url} title={pager[item].title} className={className} key={index}>
                        <FormattedMessage id={`pager:${item}`}/>
                    </Link>);
                })
            }
        </div>);
    }
}

export default connect(store => ({
    articleId: store.article.id || 0,
    menu: store.menu
}))(Pager);
