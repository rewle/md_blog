'use strict';

import React, { Component } from 'react';
import {FormattedMessage} from 'react-intl';
import * as messages from "./intl.js";

require('./copy.styl');

const author = <span itemprop="name">'Rewle'</span>;

export default class Copy extends Component {
    render() {
        let years;
        const year = (new Date()).getFullYear();

        if (year === 2016) {
            years = '2016';
        } else {
            years = `2016 — ${year}`;
        }

        return (<div itemprop="author" itemscope itemtype="http://schema.org/Person" className="copy">
            <FormattedMessage id={messages["copyright"]} values={{author, years}}/>
        </div>);
    }
}
