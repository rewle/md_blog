'use strict';

import React, { Component } from 'react';
import { connect } from 'react-redux';

require('./tags.styl');

class Tags extends Component {
    render() {
        return (<div className="tags">
            {this.props.tags.map(tag => <div key={tag} className="tags__tag">{tag}</div>)}
        </div>);
    }
}

export default connect(store => {
    return {
        tags: store.tags
    };
})(Tags);
