'use strict';

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { injectIntl, FormattedMessage } from 'react-intl';

import { article } from '../../actions';

import Toc from '../../components/toc';
import Tags from '../../components/tags';
import ArticleText from '../../components/article-text';
import ArticleMeta from '../../components/article-meta';
import Spinner from '../../components/spinner';
import DoubleCol from  '../../containers/double-col';
import title from '../../utils/title';

require('./blog.styl');

class Blog extends Component {
    _check(params, article) {
        if (article.id !== Number(params.article)) {
            this.props.actions.article(params.article);
        }
    }

    componentDidMount() {
        this._check(this.props.params, this.props.article);
    }

    componentWillReceiveProps(nextProps) {
        this._check(nextProps.params, nextProps.article);
    }

    article() {
        if (!this.props.article.content) {
            title(this.props.intl.formatMessage({ id: 'blog:title' }));
            return <Spinner/>;
        }

        title([this.props.intl.formatMessage({ id: 'blog:title' }), this.props.article.title]);
        return [
            <ArticleText key="text"/>,
            <ArticleMeta key="meta"/>
        ];
    }

    render() {
        return (<DoubleCol
            leftTilte={<FormattedMessage id="pane:toc-title"/>}
            left={
                [<Toc key="toc"/>, <Tags key="tags"/>]
            }
            right={this.article()}
        />);
    }
}

export default withRouter(connect((store, own) => {
    return {
        params: own.match.params,
        article: store.article
    };
}, dispatch => ({
    actions: {
        article: id => article(id, dispatch)
    }
}))(injectIntl(Blog)));
