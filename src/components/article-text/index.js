/* eslint react/no-danger: 0 */
'use strict';

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { injectIntl, FormattedMessage, FormattedDate } from 'react-intl';

import LDBreadcrumbs from '../ld/breadcrumbs';

require('./article-text.styl');


class Article extends Component {
    render() {
        const article = this.props.article;
        const breadcrumbs = [{
            id: "http://rewle.net/blog",
            name: this.props.intl.formatMessage({ id: 'blog:title' })
        },{
            id: `http://rewle.net/blog/${article.id}`,
            name: `${article.volume} — ${article.title}`
        }];

        return (<div className="article-text">
            <LDBreadcrumbs items={breadcrumbs}/>
            <div className="article-text__title-wrapper">
                <h1 className="article-text__volume-title">{article.volume}</h1>
                <span className="article-text__date">
                    <FormattedDate
                        value={new Date(article.date)}
                        year='numeric'
                        month='short'
                        day='numeric'
                    />
                </span>
            </div>
            <h2 className="article-text__title">{article.title}</h2>
            <div className="article-text__text" dangerouslySetInnerHTML={{ __html: article.content }}/>
        </div>
        );
    }
}

export default connect(store => ({
    article: store.article
}))(injectIntl(Article));
