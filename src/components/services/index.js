'use strict';

import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { injectIntl } from 'react-intl';

import LDBreadcrumbs from '../ld/breadcrumbs';

import DoubleCol from  '../../containers/double-col';
import title from '../../utils/title';
import ServicesForm from './form';

require('./services.styl');


class Services extends Component {
    componentDidMount() {
        title(this.props.intl.formatMessage({ id: 'services:title' }));
    }
    renderItem(service) {
        return (<div className="services__item" key={service.id}>
            <div className="services__item-title">
                {service.title}
            </div>
            <div className="services__item-subtitle">
                {service.subtitle}
            </div>
        </div>);
    }

    renderAbout() {
        return (<div className="services__about">
            <h1 className="services__title">Rewle.net</h1>
            <div className="services__description">
                <p>
                    У меня пока нет наград и значимого портфолио, но я гибче и активнее большинства агенств.
                    Я обладаю достаточным опытом решения проблем бизнеса в крупных фирмах IT сферы.
                    Часть моих мыслей изложена в заметках, тамже в них можно найти описания, применяемых мной методов.
                    Если у вас возникнут сомнения в моей компетенции, я готов провести с вами пробную сессию.
                </p><p>
                    В случае любого вашего решения, я всегда готов бескорыстно ответить на все ваши вопросы, помочь разобраться с имеющимися проблемами, даже, если вы не готовы заключить со мной договор.
                </p><p>
                    Пишите на мой <a href="mailto:rewle@yandex.ru">e-mail</a>.
                </p>
            </div>

        </div>);
    }

    getCurrentService() {
        return this.props.services.find(item => item.id === this.props.match.params.service);
    }

    renderService() {
        const service = this.getCurrentService();
        const breadcrumbs = [
            {
                id: "http://rewle.net/services",
                name: this.props.intl.formatMessage({ id: 'services:title' })
            },{
                id: `http://rewle.net/services/${service.id}`,
                name: service.title
            }
        ];
        return (<div className="services__service">
            <LDBreadcrumbs items={breadcrumbs}/>
            <h1 className="services__title">{service.title}</h1>
            <div className="services__description">{
                service.description.map((p, i) => <p key={i}>{p}</p>)
            }</div>
            <div className="services__form">
                <ServicesForm />
            </div>
        </div>);
    }

    renderRight() {
        if (this.props.match.params.service) {
            return this.renderService();
        }
        return this.renderAbout();
    }

    render() {
        const items = this.props.services.map(service => ({
            href: `/services/${service.id}`,
            node: this.renderItem(service),
            current: service.id === this.props.match.params.service
        }));

        return (<DoubleCol
            leftItems={items}
            right={this.renderRight()}
        />);
    }
}

export default withRouter(connect((store) => ({
    services: store.services
}))(injectIntl(Services)));
