import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { FormattedMessage } from 'react-intl';

import { service } from '../../../actions';

require('./form.styl')

class ServicesForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        this.state.service = this.props.current;
        this.state.csrf = this.props.csrf;
    }

    componentWillReceiveProps(nextProps) {
        this.state.service = nextProps.current;
        this.state.csrf = nextProps.csrf;
    }

    handleChange(event) {
        this.setState({[event.target.name]: event.target.value});
    }

    componentWillUpdate(nextProps, nextState) {
        if (this.state.name !== nextState.name ||
            this.state.email !== nextState.email) {
                this.props.actions.check(nextState);
            }
    }

    handleSubmit(event) {
        event.preventDefault();
        this.props.actions.send(this.state);
    }

    render() {
        const inputClass = 'services__form-input';
        const errorClass = 'services__form-invalid';
        let inputNameClass = inputClass;
        let inputEmailClass = inputClass;
        if (this.props.service.invalid) {
            if (this.props.service.invalid.name) {
                inputNameClass += ` ${errorClass}`;
            }
            if (this.props.service.invalid.email) {
                inputEmailClass += ` ${errorClass}`;
            }
        }

        return (
            <form disabled={!this.props.service.sending} className="services__form" onSubmit={this.handleSubmit} action="/api/mail" method="POST">
                <input type="hidden" name="service" value={this.props.current || ''} />
                <input type="hidden" name="csrf" value={this.props.csrf || ''} />

                <label className="services__form-row">
                    <span className="services__form-label">
                        <FormattedMessage id="services:form:name"/>
                    </span>
                    <input className={inputNameClass} type="text" name="name" value={this.state.name} onChange={this.handleChange} />
                </label>
                <label className="services__form-row">
                    <span className="services__form-label">
                        <FormattedMessage id="services:form:email"/>
                    </span>
                    <input className={inputEmailClass} type="text" name="email" value={this.state.email} onChange={this.handleChange} />
                </label>
                <label className="services__form-row">
                    <span className="services__form-label">
                        <FormattedMessage id="services:form:question"/>
                    </span>
                    <textarea name="question" className="services__form-area" onChange={this.handleChange}>{this.state.question}</textarea>
                </label>
                <button type="submit" className="services__form-submit">
                    <FormattedMessage id="services:form:submit"/>
                </button>
            </form>
        );
    }
}

export default withRouter(connect((store, own) => ({
    service: store.service,
    csrf: store.csrf,
    current: own.match.params.service
}), dispatch => ({
    actions: {
        send: form => { service(form, false, dispatch) },
        check: form => { service(form, true, dispatch) }
    }
}))(ServicesForm));
