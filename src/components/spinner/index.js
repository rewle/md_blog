'use strict';

import React, { Component } from 'react';

require('./spinner.styl');

export default class Spinner extends Component {
    render() {
        return <img className="spinner" src="/static/i/spinner.gif"/>;
    }
}
