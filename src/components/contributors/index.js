'use strict';

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { injectIntl } from 'react-intl';

import User from '../user';
import DoubleCol from  '../../containers/double-col';
import title from '../../utils/title';

require('./contributors.styl');

class Contributors extends Component {
    componentDidMount() {
        title(this.props.intl.formatMessage({ id: 'contributors:title' }));
    }

    renderContributor(user, current) {
        const className = 'contributors__contributor' + (current ? ' contributors__contributor_current' : '');
        return (
            <div className={className} key={user.login}>
                <div className="contributors__left">
                    <div className="contributors__pic">
                        <img src={user.avatar}/>
                    </div>
                </div>
                <div className="contributors__right">
                    <div className="contributors__name">
                        {user.fullName}
                    </div>
                    <div className="contributors__role">
                        {user.role}
                    </div>
                </div>
            </div>
        );
    }

    render() {
        const items = this.props.contributors.map(user => {
            const current = user.login === this.props.match.params.login;
            return {
                current: current,
                href: `/contributors/${user.login}`,
                node: this.renderContributor(user, current)
            };
        });

        return (<DoubleCol
            leftItems={items}
            right={<User {...this.props}/>}
        />);
    }
}

export default withRouter(connect((state, own) => ({
    contributors: state.contributors
}))(injectIntl(Contributors)));
