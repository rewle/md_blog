/* eslint react/no-danger: 0 */
'use strict';

import React, { Component } from 'react';

class LDBreadcrumbs extends Component {

    render() {
        const ldBreadcrumbs = {
            "@context": "http://schema.org",
            "@type": "BreadcrumbList",
            "itemListElement": this.props.items.map((item, i) => ({
                "@type": "ListItem",
                "position": i,
                "item": {
                    "@id": item.id,
                    "name": item.name
                }
            }))
        };
        return (<script type="application/ld+json" dangerouslySetInnerHTML={{
            __html: JSON.stringify(ldBreadcrumbs)
        }} />);
    }
}

export default LDBreadcrumbs;
