'use strict';

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';

import { message } from '../../actions';

require('./alert.styl');

class Alert extends Component {
    handleClose() {
        this.props.actions.close()
    }
    render() {
        return (<div className={`alert alert_${this.props.type} alert_${this.props.direction}`}>
            {this.props.text ? <FormattedMessage id={this.props.text}/> : ''}
            <div className="alert__close" onClick={this.handleClose.bind(this)}></div>
        </div>);
    }
}

export default connect(store => ({
    type: store.message.type,
    text: store.message.text
}), dispatch => ({
    actions: {
        close: () => {
            message({ type: 'empty' }, dispatch);
        }
    }
}))(Alert);
