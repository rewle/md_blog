'use strict';

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router'
import { Link } from 'react-router-dom';

require('./toc.styl');

class Toc extends Component {
    render() {
        return (<ol className="toc">
                {
                    this.props.menu.map((item, index) => (<li className="toc__item" key={index}>
                        <span className="toc__menu-header">{item.title}</span>
                        <ol className="toc__submenu">
                            {
                                item.content.map((subitem, subindex) => {

                                    let className = 'toc__subitem';
                                    if (this.props.match.url === subitem.url) {
                                        className += ' toc__subitem_current';
                                    }

                                    return (<li className={className} key={subindex}>
                                        <Link to={subitem.url} className="toc__link">
                                            {subitem.title}
                                        </Link>
                                    </li>);

                                })
                            }
                        </ol>
                    </li>))
                }
            </ol>
        );
    }
}

export default withRouter(connect(store => ({
    menu: store.menu,
    location: store.router.location
}))(Toc));
