'use strict';

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';

class MenuItem extends Component {
    render() {
        let className = 'menu__item';

        if (!this.props.current.pathname.indexOf(this.props.url)) {
            className += ' menu__item_current';
        }

        return (<li>
            <Link to={this.props.url} className={className}>
                <FormattedMessage id={this.props.title}/>
            </Link>
        </li>);
    }
}

export default MenuItem;
