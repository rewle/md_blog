'use strict';

import React, { Component } from 'react';
import MenuItem from './_item';

require('./menu.styl');

const pages = [
    {
        url: '/blog',
        title: 'menu:articles'
    },
    {
        url: '/services',
        title: 'menu:services'
    },
    {
        url: '/contributors',
        title: 'menu:team'
    }
];

export default class Menu extends Component {
    render() {
        return (<nav className="menu"><ul>
            {
                pages.map((page, index) => (
                    <MenuItem title={page.title} url={page.url} current={this.props.location} key={index}/>
                ))
            }
        </ul></nav>);
    }
}
