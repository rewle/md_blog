'use strict';

import React, { Component } from 'react';
import { renderToString } from 'react-dom/server';

import { createStore, combineReducers } from 'redux';
// import { Provider } from 'react-redux';
import { Provider, intlReducer } from 'react-intl-redux'

import { ConnectedRouter, routerMiddleware, routerReducer } from 'react-router-redux';
import StaticRouter from 'react-router-dom/StaticRouter';
import { renderRoutes } from 'react-router-config';

import {IntlProvider} from 'react-intl';
// import * as translations from './translations';
import { addLocaleData } from 'react-intl';
import ruTranslation from './translations/ru'
import ru from 'react-intl/locale-data/ru';

import routes from './utils/routes';
import * as reducers from './reducers';

addLocaleData([...ru]);

module.exports = (url, data) => {
    data.intl = {
        locale: 'ru',
        messages: ruTranslation
    };
    const store = createStore(combineReducers({
        contributors: reducers.common([]),
        backgrounds: reducers.common([]),
        service: reducers.service,
        services: reducers.common([]),
        csrf: reducers.common([]),
        menu: reducers.common([]),
        message: reducers.message,
        contributor: reducers.contributor,
        tags: reducers.tags,
        article: reducers.article,
        router: routerReducer,
        intl: intlReducer
    }), data);

    return (<Provider store={store}>
            <StaticRouter location={url} context={{}}>
                {renderRoutes(routes)}
            </StaticRouter>
        </Provider>);
}
