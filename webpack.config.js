'use strict';

const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const jsLoader = {
    test: /\.js$/,
    exclude: /node_modules/,
    use: {
        loader: 'babel-loader',
        options: {
            presets: ['react', 'es2015']
        }
    }
};

const jsLoader2 = {
    test: /(?!intl)\.js$/,
    exclude: /node_modules/,
    use: {
        loader: 'babel-loader',
        options: {
            presets: ['react', 'es2015']
        }
    }
};

const intlLoader = {
    test: /intl\.js$/,
    exclude: /node_modules/,
    use: ['react-intl-modules-loader', {
        loader: 'babel-loader',
        options: {
            presets: ['react', 'es2015']
        }
    }]
};

module.exports = [
    {
        entry: {
            head: path.join(__dirname, 'src/head.js'),
            server_content: path.join(__dirname, 'src/server_content.js'),
            server: path.join(__dirname, 'src/server.js')
        },

        target: 'node',

        output: {
            path: path.join(__dirname, 'view'),
            filename: '[name].js',
            libraryTarget: 'commonjs2'
        },

        module: {
            loaders: [
                jsLoader2,
                intlLoader,
                {
                    test: /\.styl$/,
                    use: {
                        loader: 'ignore-loader'
                    }
                }
            ]
        }
    }, {
        entry: {
            client: path.join(__dirname, 'src/client.js')
        },

        devtool: 'source-map',
        target: 'web',

        output: {
            path: path.join(__dirname, 'view'),
            filename: '[name].js'
        },

        module: {
            loaders: [
                jsLoader,
                // intlLoader,
                {
                    test: /\.styl$/,
                    loader: ExtractTextPlugin.extract({
                        use: [{
                            loader: 'css-loader',
                            options: { minimize: true }
                        }, {
                            loader: 'stylus-loader',
                            options: {
                                import: path.join(__dirname, '/src/common.styl')
                            }
                        }]
                    })
                }
            ]
        },

        plugins: [
            new ExtractTextPlugin({
                filename: '[name].css',
                allChunks: true
            })
        ]
    }];
