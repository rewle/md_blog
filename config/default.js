'use strict';

const path = require('path');
const services = require('./content/services');

module.exports = {
    cluster: {
        workerCount: 4
    },
    server: {
        port: process.env.PORT
    },
    logger: {
        level: 'info'
    },
    host: 'http://rewle.net',
    static: path.join(__dirname, '../view/'),
    staticPrefix: '/static/',
    data: path.join(__dirname, '../data/'),
    db: path.join(__dirname, '../data/db'),
    articles: path.join(__dirname, '../data/articles/'),
    users: path.join(__dirname, '../data/users/'),
    services: services,
    task: {
        baseURL: 'http://a.wunderlist.com/api/v1/',
        list: 319790572,
        connection: {
            method: 'POST',
            json: true,
            headers: {
                'X-Access-Token': process.env.W_ACCESS,
                'X-Client-ID': process.env.W_CLIENT
            }
        }
    }
};
