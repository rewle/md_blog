'use strict';

const fs = require('fs');
const path = require('path');

const sqlite3 = require('sqlite3');

const config = require('../app/lib/config');

const createArticle = 'CREATE TABLE articles (id INTEGER PRIMARY KEY,' +
    'title TEXT, ' +
    'volume TEXT, ' +
    'article TEXT, ' +
    'date TEXT)';
const createTag = 'CREATE TABLE tags (id INTEGER, tag TEXT)';
const createContributors = 'CREATE TABLE contributors (' +
    'login CHAR PRIMARY KEY, ' +
    'fullName CHAR, ' +
    'role TEXT, ' +
    'avatar TEXT)';

const dbName = path.join(config.db);

// Clean

if (fs.existsSync(dbName)) {
    fs.unlinkSync(dbName);
}

const db = new sqlite3.Database(dbName);

db.run(createArticle);
db.run(createTag);
db.run(createContributors);

db.close();
