#!/bin/bash

set -e

rm -rf data/*;

nodejs tasks/db_create.js;
nodejs tasks/db_feel.js;
nodejs tasks/sitemap.js;
