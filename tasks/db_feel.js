'use strict';

const path = require('path');
const fs = require('fs');
const crypto = require('crypto');
const sqlite3 = require('sqlite3');
const markdown = require('markdown').markdown;

const config = require('../app/lib/config');

const meta = JSON.parse(fs.readFileSync('./content/.meta.json').toString('utf8'));

const article = 'INSERT INTO articles (id, title, volume, article, date) VALUES (?, ?, ?, ?, ?)';
const tag = 'INSERT INTO tags (id, tag) VALUES (?, ?)';
const contributors = 'INSERT INTO contributors (fullName, login, role, avatar) VALUES(?, ?, ?, ?)';

const db = new sqlite3.Database(config.db);

const tmplArticle = db.prepare(article);
const tmplTag = db.prepare(tag);
const tmplContributors = db.prepare(contributors);

const volumes = {};
const menuJSON = [];

meta.volumes.forEach(volume => {
    volumes[volume.id] = volume;
    volumes[volume.id].menuItem = menuJSON.length;
    menuJSON.push({
        title: volume.title,
        content: []
    });
});

fs.mkdirSync(config.articles);
fs.mkdirSync(config.users);

db.serialize(() => {
    meta.articles.forEach((article, index) => {
        const volume = volumes[article.volume];
        const id = index + 1;
        if (!volume) {
            throw new Error(`Invalid volume for article ${index}`);
        }

        const fname = path.join('./content', volume.path, article.fname);
        const articleContent = markdown.toHTML(fs.readFileSync(fname).toString('utf8'));
        const articleFname = crypto.createHash('md5').update(articleContent).digest('hex');

        tmplArticle.run(id, article.title, volume.title, articleFname, article.date);

        article.tags.forEach(tag => {
            tmplTag.run(id, tag);
        });

        menuJSON[volume.menuItem].content.push({
            title: article.title,
            url: `/blog/${id}`,
            id: id
        });

        fs.writeFileSync(path.join(config.articles, articleFname), articleContent);
    });

    meta.contributors.forEach(user => {
        tmplContributors.run(user.fullName, user.login, user.role, `/i/users/${user.avatar}`);

        const fname = path.join('./content/users/cv', user.login) + '.md';
        const cv = markdown.toHTML(fs.readFileSync(fname).toString('utf8'));
        fs.writeFileSync(path.join(config.users, user.login), cv);
    });
});

menuJSON.filter(volume => {
    if (!volume.content.length) {
        return false;
    }
    volume.content.sort((a, b) => (a.id > b.id ? 1 : -1));
    return true;
});

fs.writeFileSync(path.join(config.data, 'menu.json'), JSON.stringify(menuJSON, 2, 2));

tmplArticle.finalize();
tmplTag.finalize();
tmplContributors.finalize();

db.close();
