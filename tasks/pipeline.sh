#!/bin/bash

set -e;

cat >.gitignore <<EOF
node_modules
EOF

./tasks/data.sh
./node_modules/.bin/webpack

npm run min-js

git config --global user.email "rewle@yandex.ru"
git config --global user.name "Alex Ilichev"

git add --all
git commit -m "release"
