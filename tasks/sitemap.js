'use strict';

const path = require('path');
const fs = require('fs');
const config = require('../app/lib/config');
const menu = path.join(config.data, 'menu.json');

const menu2urls = menu => {
    let stack = [].concat(menu);
    const urls = [];
    while (stack.length) {
        let node = stack.shift();
        if (node.content) {
            stack = stack.concat(node.content);
        }

        if (node.url) {
            urls.push(config.host + node.url);
        }
    }
    return urls;
};

const getURLXML = url => `<url>
<loc>${url}</loc>
</url>`;

const processURLS = urls => {
    const urlsXML = urls.map(getURLXML);

    return `
<?xml version="1.0" encoding="UTF-8"?>

<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
${urlsXML.join('\n')}
</urlset>`;
};

const sitemap = processURLS(menu2urls(JSON.parse(fs.readFileSync(menu))));

fs.writeFileSync(path.join(config.data, 'sitemap.xml'), sitemap);
