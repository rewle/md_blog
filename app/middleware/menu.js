'use strict';

const path = require('path');
const config = require('../lib/config');
const menu = path.join(config.data, 'menu.json');
const fs = require('fs');

module.exports = (req, res, next) => {
    res.locals.menu = JSON.parse(fs.readFileSync(menu));
    next();
};
