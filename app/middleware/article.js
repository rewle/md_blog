'use strict';

const fs = require('fs');
const path = require('path');

const config = require('../lib/config');
const proxyClb = require('../lib/proxy_clb');

const db = require('../lib/db');

const getArticle = id => (
    new Promise((resolve, reject) => {
        db.get('select * from articles where id=? limit 1;', id, proxyClb(resolve, reject));
    }).then(article => {
        if (!article) {
            return Promise.reject();
        }
        return new Promise((resolve, reject) => {
            const articlePath = path.join(config.articles, article.article);
            delete article.article;

            fs.readFile(articlePath, 'utf8', proxyClb(content => {
                article.content = content;
                resolve(article);
            }, reject));
        });
    })
);

const getTags = id => (
    new Promise((resolve, reject) => {
        db.all('select `tag` from tags where id=?;', id, proxyClb(resolve, reject));
    }).then(tags => {
        if (Array.isArray(tags)) {
            return tags.map(tag => (tag.tag));
        }
        return [tags.tag];
    })
);

module.exports = (req, res, next) => {
    const id = Number(req.params.article);

    Promise.all([
        getArticle(id),
        getTags(id)
    ]).then(data => {
        const article = data[0];

        article.tags = data[1];
        res.locals.article = article;

        next();
    }, next);
};
