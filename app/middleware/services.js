'use strict';

const config = require('../lib/config');

module.exports = (req, res, next) => {
    res.locals.services = config.services;
    next();
};
