'use strict';

const path = require('path');
const fs = require('fs');

const logger = require('../lib/logger');
const config = require('../lib/config');
const proxyClb = require('../lib/proxy_clb');

module.exports = (req, res, next) => {
    fs.readFile(
        path.join(config.users, req.params.login),
        { encoding: 'utf8' },
        proxyClb(description => {
            res.locals.contributor = {
                login: req.params.login,
                description
            };
            next();
        }, err => {
            logger.log(err);
            res.sendStatus(404);
        })
    );
};
