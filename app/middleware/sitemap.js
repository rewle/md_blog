const path = require('path');
const config = require('../lib/config');
const sitemap = path.join(config.data, 'sitemap.xml');

module.exports = (req, res) => res.sendFile(sitemap);
