'use strict';

const proxyClb = require('../lib/proxy_clb');
const db = require('../lib/db');

module.exports = (req, res, next) => {
    db.all('select * from contributors;', proxyClb(data => {
        res.locals.contributors = data.map(user => {
            user.avatar = `/static${user.avatar}`;
            return user;
        });
        next();
    }, next));
};
