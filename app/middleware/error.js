/* eslint no-unused-vars: 1 */
'use strict';

const logger = require('../lib/logger');

module.exports = (err, req, res, next) => {
    if (err.code === 'EBADCSRFTOKEN') {
        res.sendStatus(403);
        return;
    }

    logger.error('Uncaught error!', err);

    res.status(502);
    res.send('Internal error!');
};
