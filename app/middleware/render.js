'use strict';

const ReactDom = require('react-dom/server');

const head = require('../../view/head.js');
const content = require('../../view/server_content.js');
const page = require('../../view/server.js');

module.exports = (req, res) => {

    const meta = ReactDom.renderToStaticMarkup(head(res.locals));
    const serverContent = ReactDom.renderToString(content(req.url, res.locals));
    const body = ReactDom.renderToStaticMarkup(page(res.locals, serverContent));

    const data = JSON.stringify(res.locals);

    res.send(`
        <!DOCTYPE html prefix="og: http://ogp.me/ns# article: http://ogp.me/ns/article#">
        <html>
          <script>
            window.__PRELOADED_STATE__ = ${data};
          </script>
          ${meta}
          <body>
            ${body}
            <script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
            <script src="//yastatic.net/share2/share.js"></script>
            <script src="/static/client.js"></script>
          </body>
        </html>`);
};
