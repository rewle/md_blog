'use strict';

const request = require('request');

const logger = require('../lib/logger');
const config = require('../lib/config');

module.exports = (req, res) => {
    const title = `${req.body.email}-${req.body.service}-${(new Date).toISOString()}`.substr(0, 255);
    const text = `
text: ${req.body.question}
name: ${req.body.name}
email: ${req.body.email}`;

    request(Object.assign({
        uri: `${config.task.baseURL}tasks`,
        body: {
            list_id: config.task.list,
            title: title
        }
    }, config.task.connection),
    function(err, wRes, body) {
        if (!err && wRes.statusCode < 400) {
            request(Object.assign({
                uri: `${config.task.baseURL}task_comments`,
                body: {
                    task_id: body.id,
                    text: text,
                }
            }, config.task.connection),
            function(err, wRes, body) {
                if (!err && wRes.statusCode < 400) {
                    res.send(200, {done: 'ok'});
                } else {
                    logger.log('W_2', err, body);
                    res.send(502, {done: 'fail:question'});
                }
            });
        } else {
            logger.log('W_1', err, body);
            res.send(502, {done: 'fail:title'});
        }
    });
};
