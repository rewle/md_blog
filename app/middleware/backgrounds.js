'use strict';

const fs = require('fs');
const path = require('path');
const config = require('../lib/config');

const prefix = '/i/bg/';

const bgDir = path.join(config.static, prefix);
const bgList = fs.readdirSync(bgDir).map(bg => (
    path.join(config.staticPrefix, prefix, bg
)));

module.exports = (req, res, next) => {
    res.locals.backgrounds = bgList;

    next();
};
