'use strict';

module.exports = part => (req, res) => res.json(res.locals[part] || {});
