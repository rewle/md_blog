'use strict';

const cluster = require('cluster');
const path = require('path');

const config = require('./lib/config');
const logger = require('./lib/logger');

cluster.setupMaster({
    exec: path.join(__dirname, './app.js')
});

cluster.on('exit', (worker, code, signal) => {
    logger.info('worker %d died (%s).', worker.process.pid, signal || code);
    cluster.fork();
});

for (let i = 0; i < config.cluster.workerCount; i++) {
    cluster.fork();
}
