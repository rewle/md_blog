'use strict';

const path = require('path');

const express = require('express');
const compression = require('compression');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const csurf = require('csurf')({ cookie: true });

require('./lib/patchIntl');

const config = require('./lib/config');
const logger = require('./lib/logger');

const render = require('./middleware/render');
const json = require('./middleware/json');

const csrf = require('./middleware/csrf');
const error = require('./middleware/error');
const backgrounds = require('./middleware/backgrounds');
const sitemap = require('./middleware/sitemap');
const menu = require('./middleware/menu');
const services = require('./middleware/services');
const article = require('./middleware/article');
const contributors = require('./middleware/contributors');
const user = require('./middleware/user');
const task = require('./middleware/task');

const last = tree => {
    let stack = [].concat(tree);
    let index = 0;

    while (stack.length) {
        const node = stack.shift();

        if (Array.isArray(node.content)) {
            stack = stack.concat(node.content);
        }

        if (node.id > index) {
            index = node.id;
        }
    }

    return index;
};

const app = express();

const pageMiddlewares = [csrf, backgrounds, contributors, services, menu];

app.use(compression());

app.use('/static', express.static(config.static));
app.use(cookieParser());

app.get('/robots.txt', (req, res) => {
    res.sendFile(path.join(config.static, 'static/robots.txt'));
});
app.get('/sitemap.xml', sitemap);

app.get('/api/users/:login', user, json('contributor'));
app.get('/api/articles/:article', article, json('article'));

app.post('/api/task', csurf, bodyParser.json({ extended: false }), task);


app.get('/blog/', menu, (req, res) => res.redirect(302, `/blog/${last(res.locals.menu)}`));
app.get('/contributors/', contributors, (req, res) => res.redirect(302, `/contributors/${res.locals.contributors[0].login}`));
app.get('/blog/:article', csurf, pageMiddlewares, article, render);
app.get('/contributors/:login', csurf, pageMiddlewares, user, render);
app.get('*', csurf, pageMiddlewares, render);

app.use(error);

app.listen(config.server.port, () => {
    logger.info('Listening', config.server.port);
});
