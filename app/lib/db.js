'use strict';

const sqlite3 = require('sqlite3');
const config = require('../lib/config');

module.exports = new sqlite3.Database(config.db);
