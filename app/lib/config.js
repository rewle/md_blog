/* eslint global-require: 0 */
/* eslint no-console: 0 */
'use strict';

const path = require('path');
const _ = require('lodash');

const env = process.env.NODE_ENV;

const envConfFname = path.join(__dirname, `../../config/${env}`);
const defConfFname = path.join(__dirname, '../../config/default');

const config = require(defConfFname);

if (env) {
    try {
        _.merge(config, require(envConfFname));
    } catch (e) {
        console.error(process.cwd(), e);
    }
}

module.exports = config;
