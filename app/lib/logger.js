'use strict';

const winston = require('winston');
const config = require('./config');

const logger = new winston.Logger({
    level: config.logger.level,
    transports: [
        new winston.transports.Console({
            handleExceptions: true,
            formatter: options => {
                let meta = '';
                if (options.meta && Object.keys(options.meta).length) {
                    meta = JSON.stringify(options.meta);
                }
                return [
                    Date(),
                    options.level.toUpperCase(),
                    options.message || '',
                    meta
                ].join(' ');
            }
        })
    ],
    exitOnError: false
});

module.exports = logger;
