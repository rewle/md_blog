'use strict';

module.exports = (done, fail) => ((err, data) => {
    if (err) {
        fail(err);
    } else {
        done(data);
    }
});
